import React from 'react';
import ReactDOM from 'react-dom';

import Main from './RootRouter';

ReactDOM.render(<Main />, document.getElementById('react-root'));
