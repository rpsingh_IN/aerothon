import React from 'react';

import {
  Table,
} from 'semantic-ui-react';

import Pagination from './Pagination';

const renderRows = rows => rows.map(row => (
  <Table.Row>
    <Table.Cell>{row.flight_number}</Table.Cell>
    <Table.Cell>{row.airport}</Table.Cell>
    <Table.Cell>{row.maximum_altitude_in_feet}</Table.Cell>
    <Table.Cell>{row.gross_weight_in_lbs}</Table.Cell>
    <Table.Cell>{row.atmospheric_pressure_in_psi}</Table.Cell>
    <Table.Cell>{row.room_temperature_in_fahrenheit}</Table.Cell>
  </Table.Row>
));

export default ({ flights }) => (
  <Table celled>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Flight Number</Table.HeaderCell>
        <Table.HeaderCell>Airport</Table.HeaderCell>
        <Table.HeaderCell>Maximum Altitude</Table.HeaderCell>
        <Table.HeaderCell>Gross Weight</Table.HeaderCell>
        <Table.HeaderCell>Atmospheric Pressure</Table.HeaderCell>
        <Table.HeaderCell>Room Temperature</Table.HeaderCell>
      </Table.Row>
    </Table.Header>
    <Table.Body>
      {renderRows(flights.results)}
    </Table.Body>
    <Table.Footer>
      <Table.Row>
        <Table.HeaderCell colSpan="6">
          <Pagination
            totalPages={flights.count / 25}
          />
        </Table.HeaderCell>
      </Table.Row>
    </Table.Footer>
  </Table>
);
