import React from 'react';

import { Menu, Pagination } from 'semantic-ui-react';

export default props => (
  <Menu floated="right">
    <Pagination {...props} />
  </Menu>
);
