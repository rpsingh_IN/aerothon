import React, { Component } from 'react';
import {
  Container, Grid, Input,
} from 'semantic-ui-react';
import axios from 'axios';

import FlightTabel from './FlightTabel';

class FlightList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flights: {
        results: [],
      },
    };
  }

  componentDidMount() {
    axios.get('/api/aircraft/flight/')
      .then(({ data }) => {
        this.setState({ flights: data });
      })
      .catch((...args) => console.log(args));
  }

  render() {
    return (
      <Container style={{ padding: 20 }}>
        <Grid>
          <Grid.Row>
            <Grid.Column>
              <Input
                fluid
                icon="search"
                placeholder="Search Flight No, Airport etc.."
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <FlightTabel flights={this.state.flights} />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}

export default FlightList;
