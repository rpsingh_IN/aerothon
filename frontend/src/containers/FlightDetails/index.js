import React, { Component } from 'react';
import { Container, Message } from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';

import FlightForm from './FlightForm';

class FlightDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      submitted: false,
    };
  }

  onSubmit = (data) => {
    axios.post('/api/aircraft/flight/', data)
      .then((...args) => {
        console.log(args);
        this.setState({ submitted: true });
      })
      .catch((...args) => console.log(args));
  }

  render() {
    const { match } = this.props;
    return (
      <Container style={{ padding: 20 }}>
        <Message centered>
          {`${match.params.id} Program`}
        </Message>
        <FlightForm onSubmit={this.onSubmit} />
        {this.state.submitted ? (
          <Message centered color="green">
            Saved Successfully
          </Message>
        ) : null}
      </Container>
    );
  }
}

export default withRouter(FlightDetails);
