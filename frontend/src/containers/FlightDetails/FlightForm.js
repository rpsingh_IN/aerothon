import React, { Component } from 'react';
import {
  Grid, Form, Input, Dropdown, Button,
} from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';

const airPortOptions = [
  { text: 'Port1', value: '1', key: 'Port1' },
  { text: 'Port2', value: '2', key: 'Port2' },
];

class FlightForm extends Component {
  constructor(props) {
    super(props);
    const { match } = props;
    console.log(props);
    this.state = {
      formDate: {
        aircraft_model: match.params.id,
      },
    };
  }

  onChangeData = (e, { name, value }) => {
    const { formDate } = this.state;
    this.setState({
      formDate: {
        ...formDate,
        [name]: value,
      },
    });
  }

  onSubmit = () => {
    const { formDate } = this.state;
    const { onSubmit } = this.props;
    onSubmit(formDate);
  }

  render() {
    return (
      <Grid>
        <Grid.Column width="8">
          <Form>
            <Form.Field>
              <label>MSN</label>
              <Input
                placeholder="MSN"
                name="msn"
                onChange={this.onChangeData}
              />
            </Form.Field>
            <Form.Field>
              <label>Harness Length</label>
              <Input
                placeholder="Harness Length"
                label={{ basic: true, content: 'm' }}
                labelPosition="right"
                name="harness_length_in_meter"
                onChange={this.onChangeData}
              />
            </Form.Field>
            <Form.Field>
              <label>Gross Weight</label>
              <Input
                placeholder="Gross Weight"
                label={{ basic: true, content: 'kg' }}
                labelPosition="right"
                name="gross_weight_in_lbs"
                onChange={this.onChangeData}
              />
            </Form.Field>
            <Form.Field>
              <label>Atmospheric Pressure</label>
              <Input
                placeholder="Atmospheric Pressure"
                label={{ basic: true, content: 'psi' }}
                labelPosition="right"
                name="atmospheric_pressure_in_psi"
                onChange={this.onChangeData}
              />
            </Form.Field>
            <Form.Field>
              <label>Room Temperature</label>
              <Input
                placeholder="Room Temperature"
                label={{ basic: true, content: 'F' }}
                labelPosition="right"
                name="room_temperature_in_fahrenheit"
                onChange={this.onChangeData}
              />
            </Form.Field>
            <Form.Field>
              <label>Airport</label>
              <Dropdown
                fluid
                search
                selection
                placeholder="Select Airport"
                options={airPortOptions}
                name="airport"
                onChange={this.onChangeData}
              />
            </Form.Field>
          </Form>
        </Grid.Column>
        <Grid.Column width="8">
          <Form>
            <Form.Field>
              <label>Fuel Capacity on Left Wing</label>
              <Input
                placeholder="Fuel Capacity on Left Wing"
                label={{ basic: true, content: 'gal' }}
                labelPosition="right"
                name="fuel_capacity_left_wing_in_gallon"
                onChange={this.onChangeData}
              />
            </Form.Field>
            <Form.Field>
              <label>Fuel Capacity on Right Wing</label>
              <Input
                placeholder="Fuel Capacity on Right Wing"
                label={{ basic: true, content: 'gal' }}
                labelPosition="right"
                name="fuel_capacity_right_wing_in_gallon"
                onChange={this.onChangeData}
              />
            </Form.Field>
            <Form.Field>
              <label>Fuel Quantity on Left Wing</label>
              <Input
                placeholder="Fuel Quantity on Left Wing"
                label={{ basic: true, content: 'gal' }}
                labelPosition="right"
                name="fuel_quantity_left_wing_in_gallon"
                onChange={this.onChangeData}
              />
            </Form.Field>
            <Form.Field>
              <label>Fuel Quantity on Right Wing</label>
              <Input
                placeholder="Fuel Quantity on Right Wing"
                label={{ basic: true, content: 'psi' }}
                labelPosition="right"
                name="fuel_quantity_right_wing_in_gallon"
                onChange={this.onChangeData}
              />
            </Form.Field>
            <Form.Field>
              <label>Maximum altitude to be reached</label>
              <Input
                placeholder="Maximum altitude to be reached"
                label={{ basic: true, content: 'm' }}
                labelPosition="right"
                name="maximum_altitude_in_feet"
                onChange={this.onChangeData}
              />
            </Form.Field>
            <Form.Field>
              <label>Flight No</label>
              <Input
                placeholder="Flight No"
                name="flight_number"
                onChange={this.onChangeData}
              />
            </Form.Field>
          </Form>
        </Grid.Column>
        <Grid.Row>
          <Grid.Column>
            <Button
              positive
              floated="right"
              onClick={this.onSubmit}
            >
              Submit
            </Button>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}


export default withRouter(FlightForm);
