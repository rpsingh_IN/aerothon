import React from 'react';
import {
  Container, Header, Divider, Image,
} from 'semantic-ui-react';

export default () => (
  <Container style={{ padding: 20 }}>
    <Header as="h3">Section One</Header>
    <Image src="https://react.semantic-ui.com/images/wireframe/short-paragraph.png" />
    <Divider hidden />
    <Header as="h3">Section Two</Header>
    <Image src="https://react.semantic-ui.com/images/wireframe/short-paragraph.png" />
    <Divider hidden />
    <Header as="h3">Section Three</Header>
    <Image src="https://react.semantic-ui.com/images/wireframe/short-paragraph.png" />
    <Divider hidden />
    <Header as="h3">Section Four</Header>
    <Image src="https://react.semantic-ui.com/images/wireframe/short-paragraph.png" />
  </Container>
);
