import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Sidebar, Menu, Dropdown } from 'semantic-ui-react';

import axios from 'axios';

import styles from './styles.css';

class SideBarView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flightOptions: [],
    };
  }

  componentDidMount() {
    this.loadFlightModels();
  }

  loadFlightModels = () => {
    axios.get('/api/aircraft/model')
      .then(({ data }) => {
        this.setState({
          flightOptions: data.map(({ name, slug }) => ({
            text: name,
            key: name,
            value: slug,
          })),
        });
      });
  }

  render() {
    const { history, children } = this.props;
    const { state } = this;
    return (
      <Sidebar.Pushable>
        <Sidebar
          as={Menu}
          inverted
          vertical
          visible
          width="wide"
          animation="uncover"
        >
          <Menu.Item onClick={() => { history.push('/'); }}>
              Home
          </Menu.Item>
          <Menu.Item>
            <Menu.Header>Add Flight Detail</Menu.Header>
            <Dropdown
              search
              fluid
              selection
              placeholder="Select a flight"
              options={state.flightOptions}
              onChange={(e, { value }) => { history.push(`/details/${value}`); }}
            />
          </Menu.Item>
          <Menu.Item onClick={() => { history.push('/flights'); }}>
            Flights
          </Menu.Item>
        </Sidebar>
        <Sidebar.Pusher className={styles.pusher}>
          {children}
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    );
  }
}

export default withRouter(SideBarView);
