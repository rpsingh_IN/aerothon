import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Home from './containers/Home';
import SideBar from './containers/SideBar';
import FlightList from './containers/FlightList';
import FlightDetails from './containers/FlightDetails';

import styles from './styles.css';

const content = [
  <Route path="/" exact component={Home} />,
  <Route path="/details/:id" exact component={FlightDetails} />,
  <Route path="/flights" exact component={FlightList} />,
];

export default () => (
  <Router>
    <div className={styles.root}>
      <Route path="/" render={() => <SideBar>{content}</SideBar>} />
    </div>
  </Router>
);
