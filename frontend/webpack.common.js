import path from 'path';

export default {
  mode: 'none',
  cache: true,
  entry: {
    bundle: './src/index.js',
  },
  output: {
    publicPath: '/',
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env', 'react-app'],
          },
        },
        test: /\.js$/,
        exclude: /node_modules/,
      },
      {
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: true,
              localIdentName: '[name]-[local]-[hash:4]',
            },
          },
        ],
        test: /\.css$/,
      },
      {
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
            },
          },
        ],
        test: /\.(jpe?g|png|gif|svg)$/,
      },
    ],
  },
  optimization: {
    runtimeChunk: {
      name: 'vendor',
    },
    splitChunks: {
      cacheGroups: {
        default: false,
        commons: {
          test: /node_modules/,
          name: 'vendor',
          chunks: 'initial',
        },
      },
    },
  },
};
