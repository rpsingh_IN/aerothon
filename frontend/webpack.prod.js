/* eslint import/no-extraneous-dependencies: 0 */

import webpack from 'webpack';
import UglifyJsPlugin from 'uglifyjs-webpack-plugin';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import merge from 'webpack-merge';

import commonConfig from './webpack.common';

const enableBundleAnalyzer = false;

export default merge(commonConfig, {
  mode: 'production',
  optimization: {
    ...commonConfig.optimization,
    minimizer: [
      new UglifyJsPlugin(),
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify('production'),
    }),
    ...(enableBundleAnalyzer ? [
      new BundleAnalyzerPlugin(),
    ] : []),
  ],
});
