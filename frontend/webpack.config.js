const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const { NODE_ENV } = process.env;
const isProduction = NODE_ENV === 'production';

let publicPath = '/';
let bableLoaderPlugings = {};
let webpackPlugins = [];

if (isProduction) {
  publicPath = '/static/search_ui/';
  bableLoaderPlugings = {
    plugins: [
      'lodash',
      ['lodash', { id: ['semantic-ui-react'] }],
    ],
  };
  webpackPlugins = [
    new UglifyJSPlugin(),
  ];
} else {
  webpackPlugins = [
    new HtmlWebpackPlugin({
      template: 'public/index.html',
    }),
  ];
}

module.exports = {
  mode: NODE_ENV,
  entry: {
    bundle: './src/index.js',
  },
  output: {
    publicPath,
    path: path.join(__dirname, 'dists'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['react-app'],
            ...bableLoaderPlugings,
          },
        },
        test: /\.js$/,
        exclude: /node_modules/,
      },
      {
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: true,
              localIdentName: '[name]-[local]-[hash:4]',
            },
          },
        ],
        test: /\.css$/,
      },
      {
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
            },
          },
          'image-webpack-loader',
        ],
        test: /\.(jpe?g|png|gif|svg)$/,
      },
    ],
  },
  optimization: {
    runtimeChunk: {
      name: 'vendor',
    },
    splitChunks: {
      cacheGroups: {
        default: false,
        commons: {
          test: /node_modules/,
          name: 'vendor',
          chunks: 'initial',
          minSize: 1,
        },
      },
    },
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    }),
    ...webpackPlugins,
  ],
  devServer: {
    proxy: {
      '/api': 'http://localhost:8000',
    },
    historyApiFallback: true,
  },
};
