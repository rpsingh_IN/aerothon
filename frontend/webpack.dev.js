/* eslint import/no-extraneous-dependencies: 0 */

import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import merge from 'webpack-merge';

import commonConfig from './webpack.common';

export default merge(commonConfig, {
  mode: 'development',
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development'),
    }),
    new HtmlWebpackPlugin({
      template: 'public/index.html',
    }),
  ],
  devServer: {
    proxy: {
      '/api': 'http://localhost:8000',
    },
  },
});
