from django.apps import AppConfig


class AircraftConfig(AppConfig):
    name = 'aircraft'

    def ready(slef):
        import aircraft.signal_receivers  # noqa F401
