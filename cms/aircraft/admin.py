from django.contrib import admin
from aircraft.models import (
    AircraftModel,
    Aircraft,
    Flight,
)

# Register your models here.

admin.site.register([AircraftModel, Aircraft, Flight])
