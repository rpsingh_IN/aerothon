from django.conf.urls import url
from aircraft import views

urlpatterns = [
    url(r'^model/$', views.AircraftModelListApiView.as_view(), name='aircraft-model-list'),
    url(r'^flight/$', views.FlightListCreateAPIView.as_view(), name='flight-list-create'),
]
