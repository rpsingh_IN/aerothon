from aircraft.models import AircraftModel, Flight
from aircraft.serializers import (AirplaneModelModelSerializer,
                                  FlightModelSerializer)
from rest_framework import generics
from rest_framework.pagination import PageNumberPagination
from django_filters.rest_framework import DjangoFilterBackend
from django_filters.rest_framework import FilterSet, CharFilter, DateFilter


class FlightFilterSet(FilterSet):
    msn = CharFilter(field_name="aircraft__msn", lookup_expr='icontains')
    airport = CharFilter(field_name="airport", lookup_expr="icontains")
    flight_number = CharFilter(field_name="flight_number", lookup_expr="icontains")
    created_on = DateFilter(field_name="created_at", lookup_expr='icontains')

    class Meta:
        model = Flight
        fields = ['msn', 'airport', 'flight_number']


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 100


class AircraftModelListApiView(generics.ListAPIView):
    queryset = AircraftModel.objects.filter(is_deleted=False)
    serializer_class = AirplaneModelModelSerializer


class FlightListCreateAPIView(generics.ListCreateAPIView):
    queryset = Flight.objects.filter(aircraft__is_deleted=False, aircraft__aircraft_model__is_deleted=False)
    serializer_class = FlightModelSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (DjangoFilterBackend, )
    filter_class = FlightFilterSet
