from django.db.models.signals import pre_save
from aircraft.models import AircraftModel
from django.utils.text import slugify

from utils.slug_utils import create_unique_slug


def pre_save_aircraft_model_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_unique_slug(AircraftModel, slugify(instance.name))


pre_save.connect(pre_save_aircraft_model_receiver, sender=AircraftModel)
