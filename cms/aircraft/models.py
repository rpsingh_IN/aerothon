from django.db import models

# Create your models here.


class TimeStampedModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class AircraftModel(TimeStampedModel):
    slug = models.CharField(max_length=255, blank=True)
    name = models.CharField(max_length=255)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Aircraft(TimeStampedModel):
    aircraft_model = models.ForeignKey(AircraftModel, related_name='aircrafts', on_delete=models.PROTECT)
    msn = models.CharField(max_length=255, help_text='Manufacturer Serial Number', unique=True)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.msn


class Flight(TimeStampedModel):
    aircraft = models.ForeignKey(Aircraft, related_name='flights', on_delete=models.PROTECT)
    airport = models.CharField(max_length=255)
    atmospheric_pressure_in_psi = models.DecimalField(max_digits=9, decimal_places=5)
    flight_number = models.CharField(max_length=255)
    fuel_capacity_left_wing_in_gallon = models.PositiveIntegerField()
    fuel_capacity_right_wing_in_gallon = models.PositiveIntegerField()
    fuel_quantity_left_wing_in_gallon = models.PositiveIntegerField()
    fuel_quantity_right_wing_in_gallon = models.PositiveIntegerField()
    gross_weight_in_lbs = models.PositiveIntegerField()
    harness_length_in_meter = models.PositiveSmallIntegerField()
    room_temperature_in_fahrenheit = models.DecimalField(max_digits=6, decimal_places=3)
    maximum_altitude_in_feet = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.flight_number
