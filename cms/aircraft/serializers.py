from django.utils import timezone
from aircraft import models
from rest_framework import serializers


class AirplaneModelModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AircraftModel
        fields = (
            'name',
            'slug',
        )


class FlightModelSerializer(serializers.ModelSerializer):
    msn = serializers.CharField(max_length=255, write_only=True)
    aircraft_model = serializers.CharField(max_length=255, write_only=True)
    aircraft_msn = serializers.ReadOnlyField(source='aircraft.msn')
    created_at = serializers.SerializerMethodField()

    class Meta:
        model = models.Flight
        fields = (
            'msn',
            'aircraft_model',
            'aircraft_msn',
            'airport',
            'atmospheric_pressure_in_psi',
            'flight_number',
            'fuel_capacity_left_wing_in_gallon',
            'fuel_capacity_right_wing_in_gallon',
            'fuel_quantity_left_wing_in_gallon',
            'fuel_quantity_right_wing_in_gallon',
            'gross_weight_in_lbs',
            'harness_length_in_meter',
            'room_temperature_in_fahrenheit',
            'maximum_altitude_in_feet',
            'created_at',
        )

    def get_created_at(self, obj):
        return timezone.localtime(obj.created_at)

    def validate_aircraft_model(self, aircraft_model):
        aircraft_model = models.AircraftModel.objects.filter(slug=aircraft_model)
        if aircraft_model.exists():
            return aircraft_model.first()
        raise serializers.ValidationError('Invalid Aircraft Model')

    def validate(self, data):
        aircraft_model = data['aircraft_model']
        msn = data['msn']
        aircraft_qs = models.Aircraft.objects.filter(msn=msn)
        if aircraft_qs.exists():
            aircraft = aircraft_qs.first()
            if aircraft.aircraft_model != aircraft_model:
                raise serializers.ValidationError(
                    'This MSN no belonges to aircraft model {} not {}'.format(aircraft.aircraft_model, aircraft_model))
        return data

    def create(self, validated_data):
        msn = validated_data.pop('msn', None)
        aircraft_model = validated_data.pop('aircraft_model', None)
        aircraft_qs = models.Aircraft.objects.filter(msn=msn, aircraft_model=aircraft_model)
        if aircraft_qs.exists():
            aircraft = aircraft_qs.first()
        else:
            aircraft = models.Aircraft.objects.create(msn=msn, aircraft_model=aircraft_model)
        validated_data['aircraft'] = aircraft
        return super().create(validated_data)
