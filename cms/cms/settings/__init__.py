try:
    from .dev import * # noqa: F403
except Exception as ex:
    from .prod import * # noqa: F403
