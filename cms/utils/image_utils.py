from PIL import Image


def conv_thumbnail(image, size):
    img = Image.open(image.path)
    img.thumbnail(size, Image.ANTIALIAS)
    img.save(image.path, img.format, quality=85)
    return img
