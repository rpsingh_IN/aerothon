def create_unique_slug(model_class, slug):
    '''
    Return a unique slug

    :param model_class: a model class
    :param slug: a slug string
    :type model_class: any class with type 'django.db.models.base.ModelBase'
    :type slug: str
    :rtype: str
    '''
    new_slug = slug
    count = 1
    while True:
        queryset = model_class.objects.filter(slug=new_slug)
        if not queryset.exists():
            break
        new_slug = '{}-{}'.format(slug, count)
        count += 1
    return new_slug
