from django.conf.urls import url

from .views import FeedListAPIView

urlpatterns = [
    url(r'^$', FeedListAPIView.as_view(), name='feed_list'),
]
