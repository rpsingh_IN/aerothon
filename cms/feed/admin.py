from django.contrib import admin

from .models import Feed


class FeedModelAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title', 'description',)


admin.site.register(Feed, FeedModelAdmin)
