from rest_framework.generics import ListAPIView

from .models import Feed
from .serializers import FeedSerializers

from rest_framework.pagination import PageNumberPagination


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 100


class FeedListAPIView(ListAPIView):
    serializer_class = FeedSerializers
    queryset = Feed.objects.all()
    pagination_class = StandardResultsSetPagination
