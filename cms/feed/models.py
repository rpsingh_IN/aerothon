from django.db import models
from django.db.models.signals import pre_save, post_save
from django.utils import timezone
from django.utils.text import slugify

from utils.slug_utils import create_unique_slug
from utils.image_utils import conv_thumbnail


def upload_location(instance, filename):
    date = timezone.localtime(timezone.now()).strftime('%Y-%m-%d')
    return 'feed/{}/{}'.format(date, filename)


class Feed(models.Model):
    title = models.CharField(max_length=128)
    slug = models.SlugField(max_length=140)
    description = models.CharField(max_length=256)
    image = models.ImageField(upload_to=upload_location, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


def pre_save_feed_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_unique_slug(Feed, slugify(instance.title))


def post_save_feed_receiver(sender, instance, *args, **kwargs):
    if instance.image:
        conv_thumbnail(instance.image, (720, 720))


pre_save.connect(pre_save_feed_receiver, sender=Feed)
post_save.connect(post_save_feed_receiver, sender=Feed)
