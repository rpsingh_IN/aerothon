from rest_framework import serializers

from .models import Feed


class FeedSerializers(serializers.ModelSerializer):

    class Meta:
        model = Feed
        fields = '__all__'
